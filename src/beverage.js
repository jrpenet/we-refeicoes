import {addItemTbl, addMenuBebidas} from './menuDrinks'
import { removeTemp, getTemp } from './temp'

addItemTbl()
addMenuBebidas()
removeTemp()

document.querySelector('.goBack').addEventListener('click', (e) => {
    e.preventDefault()

    if(getTemp().length == 0){
        location.assign('./index.html')
    }else{
        window.history.back()
    }

})

document.querySelector('.btn-avancaForm').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('././txentrega.html')
})