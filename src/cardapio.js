import {addCardapio} from './menuHam'
import { criaTemp, removeTemp } from './temp'

addCardapio()
removeTemp()

//itens que serão mostrados de acordo com o dia da semana

let dataAmostra = new Date()
let oDia = dataAmostra.getDay()

if(oDia == 5){
    const feijoaada = document.querySelector('.sex')
    feijoaada.removeAttribute('style')
}

if(oDia == 6){
    const galinhaCab = document.querySelector('.sab')
    galinhaCab.removeAttribute('style')
}

const selectOpcoes = document.querySelector('#escolheCarne')


selectOpcoes.addEventListener('change', () => {

    if(selectOpcoes.value == 'Galinha à Cabidela'){
        criaTemp(1, 'Galinha à Cabidela', 10)
        location.assign('./conftemp.html')
    }else if(selectOpcoes.value == 'Feijoada'){
        criaTemp(1, 'Feijoada', 10)
        location.assign('./conftemp.html')
    }else if(selectOpcoes.value == '1opc'){ //selectOpt

        const mostraOp = document.querySelector('#docTable')
        mostraOp.removeAttribute('style')

        const listaOpcoes = document.querySelectorAll('.selectOpt')
        listaOpcoes.forEach((opcCarne) => {
            opcCarne.removeAttribute('type')
            opcCarne.setAttribute('type', 'radio')
        })

        const informaValor = document.querySelectorAll('.showValor')
        informaValor.forEach((valorIndividual) => {
            valorIndividual.textContent = 'R$ 10,00'
        })

    }else{

        const mostraOp = document.querySelector('#docTable')
        mostraOp.removeAttribute('style')

        const listaOpcoes = document.querySelectorAll('.selectOpt')
        listaOpcoes.forEach((opcCarne) => {
            opcCarne.removeAttribute('type')
            opcCarne.setAttribute('type', 'checkbox')
            opcCarne.setAttribute('id', 'two')
        })

        const informaValor = document.querySelectorAll('.showValor')
        informaValor.forEach((valorIndividual) => {
            valorIndividual.textContent = 'R$ 12,00'
        })
    }
})

const chks = document.querySelectorAll('.selectOpt')
chks.forEach((op) => {

    op.addEventListener('change', (e) => {
        let counter = document.querySelectorAll('input[type=checkbox]:checked').length

        for(let i = 0; i < chks.length; i++){
            if(chks[i].checked == false && counter == 2){
                let pai = chks[i]
                pai.disabled = true
            }
            if(chks[i].checked == false && counter == 1){
                let pai = chks[i]
                pai.disabled = false
                console.log('1 opcao checada')
            }
        }
    })
})
    
const btnConfirmaOpcs = document.querySelector('.confirma')
btnConfirmaOpcs.addEventListener('click', (e) => {
    e.preventDefault()

    if(selectOpcoes.value == ''){
        alert('Escolha qual opção de carne')
    } else if(selectOpcoes.value == '1opc'){
        const opcao10 = document.getElementsByName('opcoesDeCarne')
        let opcUnica = ''
        for(let i = 0; i < opcao10.length; i++){
            if(opcao10[i].checked == true){
                opcUnica += opcao10[i].value
            }
        }

        if(opcUnica == ''){
            alert('Selecione 1 opção')
        }else{
            criaTemp(1, opcUnica, 10)
            location.assign('./conftemp.html')
        }
        
    }else{
        const opcoes12 = document.getElementsByName('opcoesDeCarne')
        let duasOpcoes = ''

        for(let i = 0; i < opcoes12.length; i++){
            if(opcoes12[i].checked == true){
                duasOpcoes += opcoes12[i].value + ', '
            }
        }
        
        criaTemp(1, duasOpcoes, 12)
        location.assign('./conftemp.html')
    }

})

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})