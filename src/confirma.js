import {getPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'


cartTable()
insereCart()
rodapeCart()

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    if(getPedidos().map((x)=> x.taxa).includes('taxa')){
        location.assign('./forms.html')
    }else{
        location.assign('./txentrega.html')
    }
})