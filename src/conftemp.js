import {addAcomp} from './menuHam'
import {getTemp, removeTemp} from './temp'
import { criaPedidos } from './pedidos'

console.log(getTemp())

addAcomp()

if(getTemp()[0].produto == 'Feijoada'){
    const listaFeijao = document.querySelectorAll('.feijaoTitulo')
    //console.log(listaFeijao)
    listaFeijao.forEach((titulo) => {
        titulo.setAttribute('style', 'display: none')
    })

}

const inputGuarnicoes = document.getElementsByName('lstGuarnicao')
inputGuarnicoes[0].setAttribute('checked', 'true')

const labelDasGuarnicoes = document.querySelectorAll('.g1')

labelDasGuarnicoes.forEach((guarnicao) => {
    guarnicao.addEventListener('change', (e) => {
        for(let i = 0; i < labelDasGuarnicoes.length; i++){
            if(e.target.checked){
                if(inputGuarnicoes[i].checked == true){
                    let pai = inputGuarnicoes[i].parentNode
                    pai.setAttribute('style', 'color: yellow;')
                }
                if(inputGuarnicoes[i].checked == false){
                    let pai = inputGuarnicoes[i].parentNode
                    pai.setAttribute('style', 'color: red; text-decoration: line-through;')
                }   
            }
        }
    })
})

const mainGarnishes = document.getElementsByName('guarnicoes')

mainGarnishes.forEach((gua) => {
    gua.addEventListener('change', (e) => {
        for(let i = 0; i < mainGarnishes.length; i++){
            if(e.target.checked){
                if(mainGarnishes[i].checked == true){
                    let pai = mainGarnishes[i].parentNode
                    pai.setAttribute('style', 'color: red; text-decoration: line-through;')
                }
            }
        }

        for(let i = 0; i < mainGarnishes.length; i++){
            if(e.target.checked == false){
                if(mainGarnishes[i].checked == false){
                    let pai = mainGarnishes[i].parentNode
                    pai.setAttribute('style', 'color: #000;')
                }
            }
        }
        
    })
})



const btnConfirma = document.querySelector('#confirmaProx')
btnConfirma.addEventListener('click', (e) => {
    e.preventDefault()

    const obs = document.querySelector('#insereObs').value

    const mainGarnishes = document.getElementsByName('guarnicoes')
    let guanicao = ''

    for(let i = 0; i < mainGarnishes.length; i++){
        if(mainGarnishes[i].checked === true){
            guanicao += mainGarnishes[i].value + ', ' //guarnicoes do prato arroz, macarrao etc etc
        }   
    }

    let feijao = '' // feijao 

    for(let i = 0; i < inputGuarnicoes.length; i++){
        if(inputGuarnicoes[i].checked){
            feijao = inputGuarnicoes[i].value
            //nome do feijao escolhido
        }
    }

    if(getTemp()[0].produto == 'Feijoada' && guanicao == ''){
        criaPedidos(getTemp()[0].qtd, `Feijoada, prato completo. Obs: ${obs}`, getTemp()[0].preco)
        location.assign('./beverage.html')
    }else if(getTemp()[0].produto == 'Feijoada' && guanicao != ''){
        criaPedidos(getTemp()[0].qtd, `Feijoada, sem ${guanicao}. Obs: ${obs}`, getTemp()[0].preco)
        location.assign('./beverage.html')
    }

    if(getTemp()[0].produto != 'Feijoada' && guanicao == ''){
        criaPedidos(getTemp()[0].qtd, `${getTemp()[0].produto}, ${feijao}, prato completo. Obs: ${obs}`, getTemp()[0].preco)
        //console.log('completo')
    }else if(getTemp()[0].produto != 'Feijoada' && guanicao != ''){
        criaPedidos(getTemp()[0].qtd, `${getTemp()[0].produto}, ${feijao}, sem ${guanicao}. Obs: ${obs}`, getTemp()[0].preco)
        //console.log(guanicao)
    }
    
    removeTemp()
    location.assign('./beverage.html')

})

const retorno = document.querySelector('.btn-goBack')
retorno.addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})
