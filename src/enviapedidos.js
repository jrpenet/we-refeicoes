import {getPedidos} from './pedidos'
import {getClientes} from './cliente'

if(getPedidos().map((x)=> x.taxa).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getClientes()[0].toUpperCase()

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign(`https://wa.me/558183188893?text=Pedido+via+Cardapio+Digital%0D%0A.%0D%0A%2ANome%2A%3A+${getClientes()[0].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0D%0A.%0D%0A%2AEndereco%2A%3A+${getClientes()[1].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0D%0A.%0D%0A%2ABairro%2A%3A+${getPedidos().map((z) => z.nomeDoBairro).join('')}%0D%0A.%0D%0A%2ACelular%2A%3A+${getClientes()[2]}%0D%0A.%0D%0A%2APonto+de+referencia%2A%3A+${getClientes()[3].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0D%0A.%0D%0A%2AForma+de+pagamento%2A%3A+${getClientes()[4].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0D%0A.%0D%0A%2ATroco%2A%3A+${getClientes()[5]}%0D%0A.%0D%0A%2AObservacao%2A%3A+${getClientes()[6].normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0D%0A.%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto.normalize('NFD').replace(/[\u0300-\u036f]/g, ''), 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0D%0A.%0A%2AValor%20total%2A%3A%20R$${getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')}%0D%0A.%0A%0A`)
})

if(dia == 0){
    alert('Fechado. Funcionamos de SEG-SAB das 11h às 14h')
    location.assign('https://instagram.com/we_refeicoes')
}

if(dia != 0 && hora < 11 || dia != 6 && hora > 13){
    alert('Fechado. Funcionamos de SEG-SAB das 11h às 14h')
    location.assign('https://instagram.com/we_refeicoes')
}
