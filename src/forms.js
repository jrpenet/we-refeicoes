import {insereClientes, removeClientes} from './cliente'
import {getPedidos} from './pedidos'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

// removeClientes()

if(getPedidos().map((x)=> x.taxa).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

document.querySelector('#confirmarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    const nome = document.querySelector('#nome').value
    const endereco = document.querySelector('#endereco').value
   //const bairro = document.querySelector('#bairro').value
    const cel = document.querySelector('#celular').value
    const ref = document.querySelector('#pto_referencia').value
    const pgt = document.querySelector('#pagaEM').value
    const troco = document.querySelector('#troco').value
    const obs = document.querySelector('#observacao').value

    if(nome === '' || endereco === '' || cel === ''){ //bairro === '' || 
       alert('Os campos nome, endereço e celular são obrigatórios')
      } else {
         insereClientes(nome, endereco, cel, ref, pgt, troco, obs)
         location.assign('./enviapedidos.html')
    }

})

const insereBairro = () => {
   const bairroNoForm = getPedidos().map((z) => z.nomeDoBairro).join('')
   document.querySelector('#bairro').textContent = bairroNoForm
}

insereBairro()

const pix = document.querySelector('#pagaEM')
pix.addEventListener('change', () => {
   if(pix.value == 'pix'){
      alert('Após enviar seu pedido, faça o envio do comprovante para a chave (81) 98700-3424')
   }
})