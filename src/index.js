let data = new Date()
let dia = data.getDay()
let hora = data.getHours()

if(dia == 0){
    alert('Fechado. Funcionamos de SEG-SAB das 11h às 14h')
    location.assign('https://instagram.com/we_refeicoes')
}

if(dia != 0 && hora < 11 || dia != 6 && hora > 13){
    alert('Fechado. Funcionamos de SEG-SAB das 11h às 14h')
    location.assign('https://instagram.com/we_refeicoes')
}
