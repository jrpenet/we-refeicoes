import {criaPedidos} from './pedidos'

//tabela do menu

const addItemTbl = function (){
    const tabela = document.createElement('table')
    document.querySelector('#docBebidas').appendChild(tabela)
    
    //header da tabela
    
    const tdQT = document.createElement('th')
    tdQT.textContent = 'QTD'
    document.querySelector('table').appendChild(tdQT)
    
    const tdItem2 = document.createElement('th')
    tdItem2.textContent = 'ITEM'
    document.querySelector('table').appendChild(tdItem2)
    
    const tdPRC = document.createElement('th')
    tdPRC.textContent = 'PREÇO'
    document.querySelector('table').appendChild(tdPRC)
    
    const tdpede = document.createElement('th')
    tdpede.textContent = 'PEDIR'
    document.querySelector('table').appendChild(tdpede)
}

//gera o menu na tabela

const listaDeBebidas = [{
    nomeDaBebida: 'Coca-Cola 1L',
    valor: 5
}, {
    nomeDaBebida: 'Coca-Cola Lata',
    valor: 3.5
},{
    nomeDaBebida: 'Água SEM Gás',
    valor: 2
},{
    nomeDaBebida: 'Água COM Gás',
    valor: 2
},{
    nomeDaBebida: 'Antártica Lata',
    valor: 3.5
},{
    nomeDaBebida: 'Fanta Lata',
    valor: 3.5
}]

const addElementsBeverage = (bebida) => {
    const bebidaElements = document.createElement('tr')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const tdEl = document.createElement('td')
    const tdEl2 = document.createElement('td')
    const tdEl3 = document.createElement('td')
    const tdEl4 = document.createElement('td')
    const button = document.createElement('button')


    select.setAttribute('id', bebida.nomeDaBebida.replace(/[ 1-]/g, '').replace(/[ãá]/g, 'a').toLowerCase())
    bebidaElements.appendChild(tdEl)
    tdEl.appendChild(select)
    opt.setAttribute('value', '1')
    opt.textContent = '1'
    opt2.setAttribute('value', '2')
    opt2.textContent = '2'
    opt3.setAttribute('value', '3')
    opt3.textContent = '3'
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)


    tdEl2.textContent = bebida.nomeDaBebida
    bebidaElements.appendChild(tdEl2)

    tdEl3.setAttribute('class', 'valorDaBebida')
    tdEl3.textContent = 'R$ ' + bebida.valor.toFixed(2).replace('.', ',')
    bebidaElements.appendChild(tdEl3)

    bebidaElements.appendChild(tdEl4)
    button.textContent = 'PEDIR'
    tdEl4.appendChild(button)
    
    button.addEventListener('click', (e) => {
        const quant = parseFloat(select.value)
        const prd = bebida.nomeDaBebida
        const prc = bebida.valor
        criaPedidos(quant, prd, prc)
        location.assign('././txentrega.html')
    })
   
    return bebidaElements
}

const addMenuBebidas = () => {
    listaDeBebidas.forEach((bebida) => {
        document.querySelector('table').appendChild(addElementsBeverage(bebida))
    })
}

export {addItemTbl, addMenuBebidas, addElementsBeverage}