import {criaPedidos} from './pedidos'
import {criaTemp, getTemp, removeTemp} from './temp'

//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Galinha Guisada',
    descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
    preco: 10,
    foto: './images/logo.png'
}, {
    nomeHamburguer: 'Guisado de Boi',
    descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
    preco: 10,
    foto: './images/logo.png'
}, {
    nomeHamburguer: 'Fígado Acebolado',
    descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
    preco: 10,
    foto: './images/logo.png'
}, {
    nomeHamburguer: 'Frango à Milanesa',
    descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
    preco: 10,
    foto: './images/logo.png'
}, {
    nomeHamburguer: 'Peixe Frito',
    descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
    preco: 10,
    foto: './images/logo.png'
},{
    nomeHamburguer: 'Feijão Mulatinho',
    descricao: '',
    preco: 0,
    foto: './images/logo.png',
    tipo: 'Acompanhamento'
},{
    nomeHamburguer: 'Feijão Preto',
    descricao: '',
    preco: 0,
    foto: './images/logo.png',
    tipo: 'Acompanhamento'
},{
    nomeHamburguer: 'SEM FEIJÃO',
    descricao: '',
    preco: 0,
    foto: './images/logo.png',
    tipo: 'Acompanhamento'
}]



// ,{
//     nomeHamburguer: 'Feijão Macassar',
//     descricao: '',
//     preco: 0,
//     foto: './images/logo.png',
//     tipo: 'Acompanhamento'
// }

// {
//     nomeHamburguer: 'Galinha Guisada',
//     descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
//     preco: 10,
//     foto: './images/logo.png'
// }, {
//     nomeHamburguer: 'Sarapatel',
//     descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
//     preco: 10,
//     foto: './images/logo.png'
// }, {
//     nomeHamburguer: 'Carne de Sol',
//     descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
//     preco: 10,
//     foto: './images/logo.png'
// }, {
//     nomeHamburguer: 'Parmegiana de Frango',
//     descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
//     preco: 10,
//     foto: './images/logo.png'
// }, {
//     nomeHamburguer: 'Peixe Frito',
//     descricao: 'Acompanha: Arroz, macarrão, purê, vinagrete e salada cozida.',
//     preco: 10,
//     foto: './images/logo.png'
// }

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const lbl = document.createElement('label')
    const input = document.createElement('input')
    input.setAttribute('type', 'checkbox')
    input.setAttribute('class', 'selectOpt')
    input.setAttribute('name', 'opcoesDeCarne')
    input.setAttribute('value', lanche.nomeHamburguer)
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const subValor = document.createElement('div')
    subValor.setAttribute('class', 'showValor')
    // const btn = document.createElement('button')

    lbl.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    // subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    // btn.textContent = 'PEDIR'
    // btn.setAttribute('class', 'btnPedir')
    
    divMain.appendChild(lbl)
    lbl.appendChild(input)
    lbl.appendChild(titulo)
    lbl.appendChild(descrito)
    lbl.appendChild(subValor)
    // divMain.appendChild(btn)

    // select.addEventListener('change', (e) => {
    //     subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
    //     subValor.setAttribute('style', 'color: green;')        
    // })


    // imagem.addEventListener('click', () => {
    //     const qt = parseFloat(select.value)
    //     const prodt = lanche.nomeHamburguer
    //     const price = lanche.preco
    //     criaTemp(qt, prodt, price)

    //     let data = new Date()
    //     let dia = data.getDay()

    //     if(lanche.nomeHamburguer === 'Feijoada' && dia != 5){
    //         alert('Disponível apenas nas Sextas')
    //     }else if(lanche.nomeHamburguer === 'Galinha à Cabidela' && dia != 6){
    //         alert('Disponível apenas nos Sábados')
    //     }else if(lanche.nomeHamburguer === 'Feijoada' && dia == 5 || lanche.nomeHamburguer === 'Galinha à Cabidela' && dia == 6){
    //         removeTemp()
    //         criaPedidos(qt, prodt, price)
    //         location.assign('./beverage.html')
    //     }else{
    //         location.assign('./conftemp.html')
    //     }
    // })
    
    // btn.addEventListener('click', () => {
    //     const qt = parseFloat(select.value)
    //     const prodt = lanche.nomeHamburguer
    //     const price = lanche.preco
    //     criaTemp(qt, prodt, price)

    //     let data = new Date()
    //     let dia = data.getDay()

    //     if(lanche.nomeHamburguer === 'Feijoada' && dia != 5){
    //         alert('Disponível apenas nas Sextas')
    //     }else if(lanche.nomeHamburguer === 'Galinha à Cabidela' && dia != 6){
    //         alert('Disponível apenas nos Sábados')
    //     }else if(lanche.nomeHamburguer === 'Galinha à Cabidela' && dia == 6){
    //         //lanche.nomeHamburguer === 'Feijoada' && dia == 5 || 
    //         removeTemp()
    //         criaPedidos(qt, prodt, price)
    //         location.assign('./beverage.html')
    //     }else{
    //         location.assign('./conftemp.html')
    //     }

    // })

    return divMain
}



const addGarnishes = (elemento) => {
    const laDiv = document.createElement('div')
    const labelOpc = document.createElement('label')
    labelOpc.setAttribute('class', 'g1')
    const inputOpc = document.createElement('input')
    inputOpc.setAttribute('type', 'radio')
    inputOpc.setAttribute('name', 'lstGuarnicao')
    inputOpc.setAttribute('value', elemento.nomeHamburguer)  
    const nameOpc = document.createElement('p')
    nameOpc.setAttribute('class', 'guarnicao')
    nameOpc.textContent = elemento.nomeHamburguer 

    laDiv.appendChild(labelOpc)
    labelOpc.appendChild(inputOpc)
    labelOpc.appendChild(nameOpc)

    return laDiv
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo != 'Acompanhamento').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(addElements(lanche))
        document.querySelector('#docTable').appendChild(hrEl)
    })
}

const addAcomp = () => {
    cardapio.filter((x) => x.tipo == 'Acompanhamento').forEach((lanche) => {
        document.querySelector('#docTable2').appendChild(addGarnishes(lanche))
    })
}

export {addCardapio, addElements, addAcomp}