let temp = []

const loadTemp = function(){
    const tempJSON = sessionStorage.getItem('temp')
    
    if(tempJSON !== null){
        return JSON.parse(tempJSON)
    } else {
        return []
    }
}

const saveTemp = function(){
    sessionStorage.setItem('temp', JSON.stringify(temp))
}

//expose orders from module
const getTemp = () => temp

const criaTemp = (select, hamb, precoProduto, tx, bairro) =>{
    
    temp.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    saveTemp()
}

const removeTemp = (item) => {
    temp.splice(item, 1)
    saveTemp()
}

temp = loadTemp()

export { getTemp, criaTemp, saveTemp, removeTemp}